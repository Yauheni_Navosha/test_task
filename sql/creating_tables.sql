CREATE TABLE itemcatalogue.item (
 id serial primary key,
	name varchar,
	price numeric
);

CREATE TABLE itemcatalogue.currency_rate (
	pr_key_target_currency varchar(3) primary key unique ,
	rate numeric,
	valid_to_datetime timestamp
);