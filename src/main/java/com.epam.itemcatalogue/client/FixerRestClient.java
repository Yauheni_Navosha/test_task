package com.epam.itemcatalogue.client;

import com.epam.itemcatalogue.creator.RateCreator;
import com.epam.itemcatalogue.entity.FixerResponce;
import com.epam.itemcatalogue.entity.CurrencyRate;
import com.epam.itemcatalogue.service.CurrencyRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@EnableScheduling
public class FixerRestClient {

    private static final Logger logger = LoggerFactory.getLogger(FixerRestClient.class);

    private static final String URL = "http://data.fixer.io/api/latest?access_key=f49f5ff036537202253a78f4eb463828";

    @Autowired
    private CurrencyRateService currencyRateService;

    @Autowired
    private RestTemplate restTemplate;

    private RateCreator rateCreator = new RateCreator();

    @Scheduled(fixedDelay = 1800 * 1000L, initialDelay = 1000L)
    public void updateCurrencies() {
        FixerResponce fixerResponce = restTemplate.getForObject(URL, FixerResponce.class);
        if (!fixerResponce.isSuccess()) {
            while (!fixerResponce.isSuccess()) {
                fixerResponce = restTemplate.getForObject(URL, FixerResponce.class);
                logger.info(String.valueOf(fixerResponce));
            }
        }
        List<CurrencyRate> currencyRateList = rateCreator.createRates(fixerResponce.getRates(), fixerResponce.getBase());

        for (CurrencyRate currencyRate : currencyRateList) {
            currencyRateService.updateRate(currencyRate);
        }
        logger.info(String.valueOf(fixerResponce));
    }
}
