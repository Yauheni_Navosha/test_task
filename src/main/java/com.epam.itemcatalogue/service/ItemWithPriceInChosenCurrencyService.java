package com.epam.itemcatalogue.service;

import com.epam.itemcatalogue.dto.ItemsWithPriceInChosenCurrencyDTO;
import com.epam.itemcatalogue.entity.CurrencyRate;
import com.epam.itemcatalogue.entity.Item;
import com.epam.itemcatalogue.exception.ServiceException;
import com.epam.itemcatalogue.repository.Repository;
import com.epam.itemcatalogue.repository.specification.FindAllItemsSpecification;
import com.epam.itemcatalogue.repository.specification.ItemByIdSpecification;
import com.epam.itemcatalogue.repository.specification.Specification;
import com.epam.itemcatalogue.repository.specification.ValidSpecificCurrencyRateSpecification;
import com.epam.itemcatalogue.validator.RateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@EnableScheduling
public class ItemWithPriceInChosenCurrencyService {

    @Autowired
    @Qualifier("itemRepository")
    private Repository itemRepository;

    @Autowired
    @Qualifier("currencyRateRepository")
    private Repository currencyRateRepository;

    public ItemsWithPriceInChosenCurrencyDTO receiveAllItemsInSelectedCurrency(String targetCurrency) throws ServiceException {
        CurrencyRate currencyRate = receiveCurrencyRate(targetCurrency);

        List<Item> items = receiveAllItems();

        items.forEach(item -> item.setPrice(item.getPrice().multiply(currencyRate.getRate())) );

        return new ItemsWithPriceInChosenCurrencyDTO(items, currencyRate.getTargetCurrency());
    }

    public ItemsWithPriceInChosenCurrencyDTO receiveSingleItemInSelectedCurrency(Long id, String targetCurrency) throws ServiceException {
        CurrencyRate currencyRate = receiveCurrencyRate(targetCurrency);

        Item item = findItemById(id);

        item.setPrice(item.getPrice().multiply(currencyRate.getRate()));

        return new ItemsWithPriceInChosenCurrencyDTO(Arrays.asList(item), currencyRate.getTargetCurrency());
    }

    private CurrencyRate receiveCurrencyRate(String targetCurrency) throws ServiceException {
        Specification<CurrencyRate> validSpecificCurrencyRateSpecification =
                new ValidSpecificCurrencyRateSpecification(targetCurrency);
        Optional<CurrencyRate> currencyRateOptional =
                currencyRateRepository.querySingleResult(validSpecificCurrencyRateSpecification);
        if (currencyRateOptional.isEmpty()) {
            throw new ServiceException("Cannot receive currency rate");
        }

        CurrencyRate currencyRate = currencyRateOptional.get();
        if (!RateValidator.isCurrencyRateTimeValid(currencyRate)) {
            throw new ServiceException("Currency rate is not valid");
        }

        return currencyRate;
    }

    private List<Item> receiveAllItems() throws ServiceException {
        List<Item> items = itemRepository.query(new FindAllItemsSpecification());
        if (items.isEmpty()) {
            throw new ServiceException("Cannot receive items data");
        }
        return items;
    }

    private Item findItemById(Long id) throws ServiceException {
        Optional<Item> itemOptional = itemRepository.querySingleResult(new ItemByIdSpecification(id));
        if (itemOptional.isEmpty()) {
            throw new ServiceException("Cannot receive items data");
        }
        return itemOptional.get();
    }
}
