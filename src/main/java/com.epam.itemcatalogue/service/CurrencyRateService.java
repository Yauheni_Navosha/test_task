package com.epam.itemcatalogue.service;

import com.epam.itemcatalogue.entity.CurrencyRate;
import com.epam.itemcatalogue.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CurrencyRateService {

    @Autowired
    @Qualifier("currencyRateRepository")
    private Repository rateRepository;

    public void updateRate(CurrencyRate currencyRate) {
        rateRepository.update(currencyRate);
    }
}
