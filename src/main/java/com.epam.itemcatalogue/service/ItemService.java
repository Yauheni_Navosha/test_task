package com.epam.itemcatalogue.service;

import com.epam.itemcatalogue.entity.Item;
import com.epam.itemcatalogue.exception.ServiceException;
import com.epam.itemcatalogue.repository.Repository;
import com.epam.itemcatalogue.repository.specification.FindAllItemsSpecification;
import com.epam.itemcatalogue.repository.specification.ItemByIdSpecification;
import com.epam.itemcatalogue.repository.specification.Specification;
import com.epam.itemcatalogue.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    @Qualifier("itemRepository")
    private Repository itemRepository;

    public List<Item> findAllItems() throws ServiceException {
        Specification<Item> specification = new FindAllItemsSpecification();
        List<Item> items = itemRepository.query(specification);
        if (items.isEmpty()) {
            throw new ServiceException("Items not founded");
        }
        return items;
    }

    public void addItem(Item item) {
        itemRepository.add(item);
    }

    public void updateItem(Item item) {
        itemRepository.update(item);
    }

    public Optional<Item> findItemById(Long id) throws ServiceException {
        Specification<Item> specification = new ItemByIdSpecification(id);
        Optional<Item> itemOptional = itemRepository.querySingleResult(specification);
        if (itemOptional.isEmpty()) {
            throw new ServiceException("Item not founded");
        }
        return itemOptional;
    }

    public void deleteItemById(Long id) throws ServiceException {
        Specification<Item> specification = new ItemByIdSpecification(id);
        try {
            itemRepository.delete(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
