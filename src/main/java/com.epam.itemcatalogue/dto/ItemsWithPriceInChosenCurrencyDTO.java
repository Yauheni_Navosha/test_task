package com.epam.itemcatalogue.dto;

import com.epam.itemcatalogue.entity.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ItemsWithPriceInChosenCurrencyDTO {
    private List<Item> itemList;
    private String currencyName;
}
