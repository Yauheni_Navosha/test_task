package com.epam.itemcatalogue.controller;

import com.epam.itemcatalogue.dto.ItemsWithPriceInChosenCurrencyDTO;
import com.epam.itemcatalogue.entity.Item;
import com.epam.itemcatalogue.exception.ServiceException;
import com.epam.itemcatalogue.service.ItemService;
import com.epam.itemcatalogue.service.ItemWithPriceInChosenCurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@RestController
public class ItemRestController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private ItemWithPriceInChosenCurrencyService itemWithPriceInChosenCurrencyService;

    @GetMapping(value = "/item/currency/{targetCurrency}")
    public ResponseEntity<ItemsWithPriceInChosenCurrencyDTO> listAllItems(@PathVariable String targetCurrency) throws ServiceException {
        ItemsWithPriceInChosenCurrencyDTO items = itemWithPriceInChosenCurrencyService.
                receiveAllItemsInSelectedCurrency(targetCurrency);
        return new ResponseEntity<ItemsWithPriceInChosenCurrencyDTO>(items, HttpStatus.OK);
    }

    @GetMapping(value = "/item/{id}/currency/{targetCurrency}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemsWithPriceInChosenCurrencyDTO> getSingleItem(@PathVariable("id") long id,
                                                                           @PathVariable String targetCurrency) throws ServiceException {
        ItemsWithPriceInChosenCurrencyDTO item = itemWithPriceInChosenCurrencyService.
                    receiveSingleItemInSelectedCurrency(id, targetCurrency);

        return new ResponseEntity<ItemsWithPriceInChosenCurrencyDTO>(item, HttpStatus.OK);
    }

    @PostMapping(value = "/item/")
    public ResponseEntity<Void> createItem(@RequestBody Item item, UriComponentsBuilder ucBuilder) {
        itemService.addItem(item);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ucBuilder.path("/item/{id}").buildAndExpand(item.getId()).toUri());
        return new ResponseEntity<Void>(httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping(value = "/item/{id}")
    public ResponseEntity<Item> updateItem(@PathVariable("id") long id, @RequestBody Item item) throws ServiceException {
        Optional<Item> currentItemOpt = itemService.findItemById(id);
        Item currentItem = currentItemOpt.get();
        currentItem.setName(item.getName());
        currentItem.setPrice(item.getPrice());
        itemService.updateItem(currentItem);

        return new ResponseEntity<Item>(currentItem, HttpStatus.OK);
    }

    @DeleteMapping(value = "item/{id}")
    public ResponseEntity<Item> deleteItem(@PathVariable("id") long id) throws ServiceException {
        itemService.deleteItemById(id);
        return new ResponseEntity<Item>(HttpStatus.OK);
    }

}
