package com.epam.itemcatalogue.controller;

import com.epam.itemcatalogue.client.FixerRestClient;
import com.epam.itemcatalogue.error.ApiErrorResponse;
import com.epam.itemcatalogue.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalRestExceptionHandler.class);

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<Object> handleRunTimeException(RuntimeException e) {

        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);

    }

    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<Object> handleNotFoundException(ServiceException e) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);

    }

    private ResponseEntity<Object> error(HttpStatus status, Exception e) {
        ApiErrorResponse response = new ApiErrorResponse.ApiErrorResponseBuilder()
                .withStatus(status)
                .withError_code(status.NOT_FOUND.name())
                .withMessage(e.getLocalizedMessage())
                .withDetail(e.getMessage())
                .build();
        logger.error("Exception : ", e);
        return new ResponseEntity<>(response, response.getStatus());

    }

}
