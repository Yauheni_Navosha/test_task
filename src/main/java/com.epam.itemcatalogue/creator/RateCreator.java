package com.epam.itemcatalogue.creator;

import com.epam.itemcatalogue.entity.CurrencyRate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RateCreator {

    public List<CurrencyRate> createRates(Map<String, BigDecimal> map, String base) {
        List<CurrencyRate> currencyRateList = new ArrayList<>();
        Set<Map.Entry<String, BigDecimal>> pairs = map.entrySet();
        LocalDateTime now = LocalDateTime.now().plusHours(1);
        for (Map.Entry<String, BigDecimal> pair : pairs) {
            currencyRateList.add(new CurrencyRate(pair.getKey().toUpperCase(), pair.getValue(), now));
        }
        currencyRateList.add(new CurrencyRate(base.toUpperCase(), new BigDecimal("1"), now));
        return currencyRateList;
    }
}
