package com.epam.itemcatalogue.repository.specification;

import com.epam.itemcatalogue.entity.CurrencyRate;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public class ValidSpecificCurrencyRateSpecification implements Specification<CurrencyRate> {

    private String targetCurrency;

    @Override
    public String toSql() {
        return "SELECT * from itemcatalogue.currency_rate " +
                "where pr_key_target_currency = UPPER(?)";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(targetCurrency);
    }
}
