package com.epam.itemcatalogue.repository.specification;

import com.epam.itemcatalogue.entity.Item;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
@AllArgsConstructor
public class ItemByIdSpecification implements Specification<Item> {

    private Long id;

    @Override
    public String toSql() {
        return "SELECT ITEM.* from itemcatalogue.item where id = ?";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }
}
