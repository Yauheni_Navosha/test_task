package com.epam.itemcatalogue.repository.specification;

import java.util.List;

public interface Specification<T> {

    String toSql();

    List<Object> getParameters();
}
