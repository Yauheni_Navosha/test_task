package com.epam.itemcatalogue.repository.specification;

import com.epam.itemcatalogue.entity.Item;

import java.util.Arrays;
import java.util.List;

public class FindAllItemsSpecification implements Specification<Item> {
    @Override
    public String toSql() {
        return "SELECT ITEM.* from itemcatalogue.item";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList();
    }
}
