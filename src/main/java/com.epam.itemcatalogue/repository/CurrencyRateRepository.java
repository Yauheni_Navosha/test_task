package com.epam.itemcatalogue.repository;

import com.epam.itemcatalogue.entity.CurrencyRate;
import com.epam.itemcatalogue.exception.RepositoryException;
import com.epam.itemcatalogue.repository.specification.Specification;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository(value = "currencyRateRepository")
public class CurrencyRateRepository extends AbstractRepository<CurrencyRate> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<CurrencyRate> querySingleResult(Specification specification) {
        String sql = specification.toSql();
        Query query = entityManager.createNativeQuery(specification.toSql(), CurrencyRate.class);
        Optional<CurrencyRate> optionalCurrencyRate = super.querySingleResult(query, specification.getParameters());
        return optionalCurrencyRate;
    }

    @Override
    public List<CurrencyRate> query(Specification specification) {
        String sql = specification.toSql();
        Query query = entityManager.createNativeQuery(specification.toSql(), CurrencyRate.class);
        return super.query(query, specification.getParameters());
    }

    @Override
    @Transactional
    public void add(CurrencyRate currencyRate) {
        super.add(currencyRate);
    }

    @Override
    @Transactional
    public void update(CurrencyRate currencyRate) {
        super.update(currencyRate);
    }

    @Override
    @Transactional
    public void delete(Specification specification) {
        Optional<CurrencyRate> rate = querySingleResult(specification);
        if (!rate.isEmpty()) {
            super.delete(rate.get());
        }
    }
}
