package com.epam.itemcatalogue.repository;

import com.epam.itemcatalogue.entity.Identifable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Repository
public abstract class AbstractRepository<T extends Identifable> implements Repository<T>{

    public Optional<T> querySingleResult(Query query, List<Object> params) {
        for(int i = 0; i < params.size(); i++) {
            query.setParameter(i + 1, params.get(i));
        }
        List<T> list = query.getResultList();
        if (!list.isEmpty()) {
            return Optional.of(list.get(0));
        }
        return Optional.empty();
    }

    public List<T> query(Query query, List<Object> params) {
        for(int i = 0; i < params.size(); i++) {
            query.setParameter(i + 1, params.get(i));
        }
        return query.getResultList();

    }

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void add(T item) {
        entityManager.persist(item);
    }

    @Transactional
    public void update(T item) {
        entityManager.merge(item);
    }

    @Transactional
    public void delete(T item) {
        entityManager.remove(item);
    }

}
