package com.epam.itemcatalogue.repository;

import com.epam.itemcatalogue.entity.Identifable;
import com.epam.itemcatalogue.repository.specification.Specification;
import com.epam.itemcatalogue.exception.RepositoryException;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Repository
public interface Repository<T extends Identifable> {

    void add(T t);
    Optional<T> querySingleResult(Specification specification);
    List<T> query(Specification specification);
    void update(T t);
    void delete(Specification specification) throws RepositoryException;

}
