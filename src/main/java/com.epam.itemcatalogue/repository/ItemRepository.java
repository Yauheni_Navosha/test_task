package com.epam.itemcatalogue.repository;

import com.epam.itemcatalogue.entity.Item;
import com.epam.itemcatalogue.repository.specification.Specification;
import com.epam.itemcatalogue.exception.RepositoryException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository(value = "itemRepository")
public class ItemRepository extends AbstractRepository<Item> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Item> querySingleResult(Specification specification) {

        String sql = specification.toSql();
        Query query = entityManager.createNativeQuery(specification.toSql(), Item.class);
        return super.querySingleResult(query, specification.getParameters());
    }

    @Override
    public List<Item> query(Specification specification) {
        String sql = specification.toSql();
        Query query = entityManager.createNativeQuery(specification.toSql(), Item.class);
        return super.query(query, specification.getParameters());
    }

    @Override
    @Transactional
    public void add(Item item) {
        super.add(item);
    }

    @Override
    @Transactional
    public void update(Item item) {
        super.update(item);
    }

    @Override
    @Transactional
    public void delete(Specification specification) throws RepositoryException {
        Optional<Item> item = querySingleResult(specification);
        if (item.isEmpty()) {
            throw new RepositoryException("Item is not exist");
        }
        super.delete(item.get());
    }

}
