package com.epam.itemcatalogue.exception;

public class RepositoryException extends Exception {

    public RepositoryException(String message) {
        super(message);
    }
}
