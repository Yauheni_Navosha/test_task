package com.epam.itemcatalogue.validator;

import com.epam.itemcatalogue.entity.CurrencyRate;

import java.time.LocalDateTime;

public class RateValidator {

    public static final boolean isCurrencyRateTimeValid(CurrencyRate currencyRate) {
        return currencyRate.getValidToDatetime().compareTo(LocalDateTime.now()) >= 0 ?
                true : false;
    }
}
