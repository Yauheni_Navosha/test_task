package com.epam.itemcatalogue.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "itemcatalogue.currency_rate")
@Getter
@Setter
public class CurrencyRate implements Identifable {

    @Id
    @Column(name="pr_key_target_currency", unique=true)
    private String targetCurrency;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "valid_to_datetime", nullable = false)
    private LocalDateTime validToDatetime;
}
