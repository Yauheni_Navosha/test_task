package com.epam.itemcatalogue.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor @NoArgsConstructor
@Table(name = "itemcatalogue.item")
@Getter @Setter
@ToString
public class Item implements Identifable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

}
