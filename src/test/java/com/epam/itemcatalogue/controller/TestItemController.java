package com.epam.itemcatalogue.controller;

import com.epam.itemcatalogue.config.WebAppInitializaer;
import com.epam.itemcatalogue.dto.ItemsWithPriceInChosenCurrencyDTO;
import com.epam.itemcatalogue.entity.Item;
import com.epam.itemcatalogue.service.ItemService;
import com.epam.itemcatalogue.service.ItemWithPriceInChosenCurrencyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppInitializaer.class})
@WebAppConfiguration
public class TestItemController {
    private static final String TARGET_CURRENCY = "USD";

    private MockMvc mockMvc;

    private Item item1 = new Item(1L, "item1", new BigDecimal("5.005"));
    private Item item2 = new Item(2L, "item2", new BigDecimal("1.001"));
    @Mock
    private ItemService itemService;

    @Mock
    private ItemWithPriceInChosenCurrencyService itemWithPriceInChosenCurrencyService;

    @InjectMocks
    private ItemRestController itemRestController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(itemRestController)
                .build();
    }

    @Test
    public void shouldReturnAllItemsWithPriceInChosenCurrencyDTO() throws Exception {
        List<Item> items = Arrays.asList(item1, item2);
        ItemsWithPriceInChosenCurrencyDTO itemDTO = new ItemsWithPriceInChosenCurrencyDTO(items, TARGET_CURRENCY);
        when(itemWithPriceInChosenCurrencyService.receiveAllItemsInSelectedCurrency(TARGET_CURRENCY)).thenReturn(itemDTO);
        mockMvc.perform(get("/item/currency/{TARGET_CURRENCY}", TARGET_CURRENCY))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.itemList", hasSize(2)))
                .andExpect(jsonPath("$.itemList[0].id", is(1)))
                .andExpect(jsonPath("$.itemList[0].name", is(item1.getName())))
                .andExpect(jsonPath("$.itemList[0].price").value(item1.getPrice()))
                .andExpect(jsonPath("$.itemList[1].id", is(2)))
                .andExpect(jsonPath("$.itemList[1].name", is(item2.getName())))
                .andExpect(jsonPath("$.itemList[1].price").value(item2.getPrice()))
                .andExpect(jsonPath("$.currencyName", is(TARGET_CURRENCY)));
        verify(itemWithPriceInChosenCurrencyService, times(1))
                .receiveAllItemsInSelectedCurrency(TARGET_CURRENCY);
        verifyNoMoreInteractions(itemWithPriceInChosenCurrencyService);
    }

    @Test
    public void shouldReturnSingleItemWithPriceInChosenCurrencyDTO() throws Exception {
        List<Item> items = Arrays.asList(item1);
        ItemsWithPriceInChosenCurrencyDTO itemDTO = new ItemsWithPriceInChosenCurrencyDTO(items, TARGET_CURRENCY);
        when(itemWithPriceInChosenCurrencyService.receiveSingleItemInSelectedCurrency(1L, TARGET_CURRENCY))
                .thenReturn(itemDTO);
        mockMvc.perform(get("/item/{id}/currency/{TARGET_CURRENCY}", 1L, TARGET_CURRENCY))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.itemList", hasSize(1)))
                .andExpect(jsonPath("$.itemList[0].id", is(1)))
                .andExpect(jsonPath("$.itemList[0].name", is(item1.getName())))
                .andExpect(jsonPath("$.itemList[0].price").value(item1.getPrice()))
                .andExpect(jsonPath("$.currencyName", is(TARGET_CURRENCY)));
        verify(itemWithPriceInChosenCurrencyService, times(1))
                .receiveSingleItemInSelectedCurrency(1L, TARGET_CURRENCY);
        verifyNoMoreInteractions(itemWithPriceInChosenCurrencyService);
    }

    @Test
    public void shouldSuccessfullyCreateItem() throws Exception {
        Item item = new Item(1L, "item", new BigDecimal("123.123"));

        mockMvc.perform(
                post("/item/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(item)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/item/1"));
    }

    @Test
    public void shouldSuccessfullyUpdateItem() throws Exception {
        Item item = new Item(1L, "item", new BigDecimal("123.123"));

        when(itemService.findItemById(item.getId())).thenReturn(Optional.of(item));
        doNothing().when(itemService).addItem(item);

        mockMvc.perform(
                put("/item/{id}", item.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(item)))
                .andExpect(status().isOk());

        verify(itemService, times(1)).findItemById(item.getId());
        verify(itemService, times(1)).updateItem(item);
        verifyNoMoreInteractions(itemService);
    }

    @Test
    public void shouldSuccessfullyDeleteItem() throws Exception {
        Item item = new Item(1L, "item", new BigDecimal("123.123"));
        doNothing().when(itemService).deleteItemById(item.getId());

        mockMvc.perform(
                delete("/item/{id}", item.getId()))
                .andExpect(status().isOk());

        verify(itemService, times(1)).deleteItemById(item.getId());
        verifyNoMoreInteractions(itemService);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
